@extends('layouts.app')
@section('pageTitle', 'Painel de Controle')
@section('content')

<div>
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">Painel de Controle</div>

            <div class="panel-body">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if($permission == 1)
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <input type="button" class="btn btn-danger botons_dash esp" value="Acesso ao Dashboard" onclick="javascript: location.href='dashboard';">
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="button" class="btn btn-warning botons_dash esp" value="Gerenciar Usuários" onclick="javascript: location.href='user';">
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <input type="button" class="btn btn btn-primary botons_dash esp_um" value="Produtos" onclick="javascript: location.href='product';" />
                    </div> 
                    <div class="col-sm-12 col-md-6">
                        <input type="button" class="btn btn-success botons_dash" value="Fornecedores" onclick="javascript: location.href='supplier';" />
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<footer>
</footer>