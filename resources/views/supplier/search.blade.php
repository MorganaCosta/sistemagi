@extends('layouts.app')
@section('pageTitle', 'Pesquisa fornecedores')
@section('content')
<div class="container">
	<h3 style="float: left;">Fornecedores encontrados:</h3>
	<div class="row" align="right">			
		<a href="/supplier">
			<button class="btn btn-default">
				Volta
			</button>
		</a>
	</div>

    <div class="row col-md-10 col-md-offset-1 container">
<!-- TABLE -->
 		<table class="table">
			<tr style="background-color: #3097d1; color: white;">
				<th>Nome</th>
				<th>CNPJ</th>
				<th>Endereço</th>
				<th></th>
				<th colspan="3">Ações</th>
			</tr>
			@foreach($suppliers as $supplier)
			<tr>
				<td>{{$supplier->name}}</td>
				<td>{{$supplier->cnpj}}</td>
				<td colspan="2">{{$supplier->address}}</td>
				<td>
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editSupplier{{$supplier->id}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
			
<!-- MODAL EDIT suppliers -->
					<div class="modal fade" id="editSupplier{{$supplier->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 class="modal-title" id="myModalLabel">Editar fornecedor</h3>
								</div>
								<form action="/supplier/edit" method="post">
								{{ csrf_field() }}
									<div class="modal-body">
										<div class="form-group">
											<input type="hidden" name="id" value="{{$supplier->id}}">
											<label>Nome</label>
											<input type="text" name="name" class="form-control" value="{{$supplier->name}}" placeholder="Nome" autofocus>
											<label>CNPJ</label>
											<input type="text" name="cnpj" class="form-control" value="{{$supplier->cnpj}}" placeholder="Descrição">
											<label>Endereço</label>
											<input type="text" name="address" class="form-control" value="{{$supplier->address}}" placeholder="Custo">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
										<input type="submit" class="btn btn-primary" value="Confirmar">
									</div>
								</form>
							</div>
						</div>
					</div>
<!--//MODAL EDIT suppliers -->
	</td>
	<td>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSupplier{{$supplier->id}}"> <i class="fa fa-trash-o" aria-hidden="true"></i></button>
<!-- MODAL DELETE suppliers -->
					<div class="modal fade" id="deleteSupplier{{$supplier->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 class="modal-title" id="myModalLabel">Editar fornecedor</h3>
								</div>
								<form action="/supplier/delete" method="post">
								{{ csrf_field() }}
									<div class="modal-body">
										<p>Você quer deletar esse fornecedor?
											<small>(Apenas delete se você não tiver produtos desse fornecedor)</small>
										</p>
									</div>
									<div class="modal-footer">
										<input type="hidden" name="id" value="{{$supplier->id}}">
										<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
										<input type="submit" class="btn btn-primary" value="Sim">
									</div>
								</form>
							</div>
						</div>
					</div>
<!--//MODAL DELETE suppliers -->
				</td>
				<td>
					<a href="/product/supplier/{{$supplier->id}}">
						<button type="button" class="btn btn-default">
							Produtos
						</button>
					</a>		
				</td>
			</tr>

			@endforeach
		</table>
<!-- //TABLE -->
	</div>
<!-- MODAL NEW SUPPLIER -->
	<div class="modal fade" id="newSupplier" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title" id="myModalLabel">Registrar novo fornecedor</h3>
				</div>
				<form action="/supplier/insert" method="post">
				{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group">
							<label>Nome</label>
							<input type="text" name="name" class="form-control" placeholder="Nome" autofocus>
							<label>CNPJ</label>
							<input type="text" name="cnpj" class="form-control" placeholder="CNPJ">
							<label>Endereço</label>
							<input type="text" name="address" class="form-control" placeholder="Endereço">
						
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
						<input type="submit" class="btn btn-primary" value="Confirmar">
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- //MODAL NEW SUPPLIER -->	
</div>
@endsection
