@extends('layouts.app')
@section('pageTitle', 'Produtos')
@section('content')
<div class="container">

	<script>
		function goBack() {
		    window.history.back();
		}
	</script>

	<div class="row" align="right">
		<h3 style="float: left; margin-top: 8px">
		 {{$bySupplier->name}} ({{$bySupplier->cnpj}})	
		</h3>	
		<a href="#ProductSoldOut">
			<button class="btn btn-info" style="float: right; margin-right: 6px;">
				Esgotados
			</button>
		</a>

		<button class="btn btn-default" style="float: right; margin-right: 6px; margin-bottom: 10px" onclick="goBack()">
			Volta
		</button>

	</div>
    <div class="row container panel col-md-10 col-md-offset-1">
    	<div class="row">
			<h3 id="´" style="float: left; margin-top: 8px;">
				&nbsp;&nbsp;&nbsp;Produtos:
			</h3>	
		</div>
		<div class="row" style="margin-left: 0px">
		<table class="table">
			<tr>
				<th>Nome</th>
				<th>Descrição</th>
				<th>Custo</th>
				<th>Quantidade</th>
				<th>Ações</th>
			</tr>
			@foreach($products as $product)
			@if($product->quantity > 0)
			<tr>
				<td>{{$product->name}}</td>
				<td>{{$product->description}}</td>
				<td>R${{$product->cost}}</td>
				<td>{{$product->quantity}}</td>
				<td>
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#unavailableProduct{{$product->id}}">
						Indisponivel
					</button>

<!-- MODAL Unavailable PRODUCTS -->					
				<div class="modal fade" id="unavailableProduct{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 class="modal-title" id="myModalLabel">Produto indisponivel</h3>
								</div>
								<form action="/product/debit" method="post">
								{{ csrf_field() }}
									<div class="modal-body">
										<div class="form-group">
											<input type="hidden" name="id" value="{{$product->id}}">
											<label>Quantidade</label>
											<input type="number" name="quantity" min="0" max="{{$product->quantity}}" class="form-control" value="0">
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
										<input type="submit" class="btn btn-primary" value="Confirm">
									</div>
								</form>
							</div>
						</div>
				</div>
<!--//MODAL Unavailable PRODUCTS -->	
					<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editProduct{{$product->id}}">Editar</button>

<!-- MODAL EDIT PRODUCTS -->
					<div class="modal fade" id="editProduct{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 class="modal-title" id="myModalLabel">Editar produto</h3>
								</div>
								<form action="/product/edit" method="post">
								{{ csrf_field() }}
									<div class="modal-body">
										<div class="form-group">
											<input type="hidden" name="id" value="{{$product->id}}">
											<label>Nome</label>
											<input type="text" name="name" class="form-control" value="{{$product->name}}" placeholder="Nome" autofocus>
											<label>Descrição</label>
											<input type="text" name="description" class="form-control" value="{{$product->description}}" placeholder="Descrição">
											<label>Custo</label>
											<input type="text" name="cost" class="form-control" value="{{$product->cost}}" placeholder="Custo">
											<label>Quantidade</label>
											<input type="number" name="quantity" min="0" class="form-control" value="{{$product->quantity}}">
											<label>Fornecedor</label>
											<select name="supplier" class="form-control">
												@foreach($suppliers as $supplier)
												<option value="{{$supplier->id}}" {{$product->supplier_id == $supplier->id ? "selected" : ""}}>
													{{$supplier->name}} ({{$supplier->cnpj}})
												</option>
												@endforeach
												
											</select>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
										<input type="submit" class="btn btn-primary" value="Confirm">
									</div>
								</form>
							</div>
						</div>
					</div>
<!-- //MODAL EDIT PRODUCTS -->
				</td>
			</tr>
			@endif

			@endforeach
		</table>
	</div>
</div>
<!-- PRODUCTS SOLD OUT -->	
	<div class="row container panel col-md-10 col-md-offset-1">
		<div class="row">
			<h3 id="ProductSoldOut" style="float: left; margin-top: 8px;">
				&nbsp;&nbsp;&nbsp;Esgotado:
			</h3>	
			</div>
	    <div class="row" style="margin-left: 0px">
					<table class="table">
						<tr>
							<th>Nome</th>
							<th>Descrição</th>
							<th>Custo</th>
							<th>Quantidade</th>
							<th colspan="3">Ações</th>
						</tr>
						@foreach($products as $product)
						@if($product->quantity == 0)
						<tr>
							<td>{{$product->name}}</td>
							<td>{{$product->description}}</td>
							<td>R${{$product->cost}}</td>
							<td>{{$product->quantity}}</td>
							<td>
								<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editProductSoldOut{{$product->id}}">
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</button>

<!-- MODAL EDIT PRODUCTS -->
								<div class="modal fade" id="editProductSoldOut{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h3 class="modal-title" id="myModalLabel">Editar produto</h3>
											</div>
											<form action="/product/edit" method="post">
											{{ csrf_field() }}
												<div class="modal-body">
													<div class="form-group">
														<input type="hidden" name="id" value="{{$product->id}}">
														<label>Nome</label>
														<input type="text" name="name" class="form-control" value="{{$product->name}}" placeholder="Nome" autofocus>
														<label>Descrição</label>
														<input type="text" name="description" class="form-control" value="{{$product->description}}" placeholder="Descrição">
														<label>Custo</label>
														<input type="text" name="cost" class="form-control" value="{{$product->cost}}" placeholder="Custo">
														<label>Quantidade</label>
														<input type="number" name="quantity" min="0" class="form-control" value="{{$product->quantity}}">
														<label>Fornecedor</label>
														<select name="supplier" class="form-control">
															@foreach($suppliers as $supplier)
															<option value="{{$supplier->id}}" {{$product->supplier_id == $supplier->id ? "selected" : ""}}>
																{{$supplier->name}} ({{$supplier->cnpj}})
															</option>
															@endforeach
															
														</select>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
													<input type="submit" class="btn btn-primary" value="Confirm">
												</div>
											</form>
										</div>
									</div>
								</div>
<!--//Modal edit Product-->
							</td>
						</tr>
						@endif

						@endforeach
					</table>
	</div>
<!--Modal Product sold out-->
	<div class="modal fade" id="ProductsSoldOut" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title" id="myModalLabel">Produtos esgotados</h3>
				</div>

					
				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal">Confirma</button>
				</div>
			</div>
		</div>
<!--//Modal Product sold out-->

	</div>
<!--Modal NEW Product-->
	<div class="modal fade" id="newPoduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title" id="myModalLabel">Registrar novo produto</h3>
				</div>
				<form action="/product/insert" method="post">
				{{ csrf_field() }}
					<div class="modal-body">
						<div class="form-group">
							<label>Nome</label>
							<input type="text" name="name" class="form-control" placeholder="Nome" autofocus>
							<label>Descrição</label>
							<input type="text" name="description" class="form-control" placeholder="Descrição">
							<label>Custo</label>
							<input type="text" name="cost" class="form-control" placeholder="Custo">
							<label>Quantidade</label>
							<input type="number" name="quantity" min="0" class="form-control">
							<label>Fornecedor</label>
							<select name="supplier" class="form-control">
								<option value="" selected>Selecione</option>

								@foreach($suppliers as $supplier)
								<option value="{{$supplier->id}}">
									{{$supplier->name}} ({{$supplier->cnpj}})
								</option>
								@endforeach

							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
						<input type="submit" class="btn btn-primary" value="Confirm">
					</div>
				</form>
			</div>
		</div>
	</div>	

<!--..Modal NEW product-->
</div>
@endsection
