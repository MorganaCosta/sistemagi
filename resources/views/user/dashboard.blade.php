@extends('layouts.app')
@section('pageTitle', 'Produtos')
@section('content')
<div class="container col-md-8 col-md-offset-2">

	<div class="row">
		<h3>
			Dados e informação:
		</h3>
		
	</div>
    <div class="row panel">
		<table class="table">
			<tr style="background-color: #3097d1; color: white;">
				<th>Nome</th>
				<th>Descrição</th>
				<th>Custo</th>
				<th>Quantidade</th>
				<th>Total</th>
			</tr>
			@foreach($products as $product)
			@if($product->quantity > 0)

			<tr>
				<td>{{$product->name}}</td>
				<td>{{$product->description}}</td>
				<td>R${{$product->cost}}</td>
				<td>{{$product->quantity}}</td>
				<td>
					<b>R${{($product->cost * $product->quantity)}}</b>
				</td>
			</tr>
			@endif

			@endforeach
		</table>
		<h3> Total do preço dos produtos: R${{$total}}</h3>
	</div>

</div>
@endsection
