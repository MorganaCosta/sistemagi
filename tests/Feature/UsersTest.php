<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\User;

class UsersTest extends TestCase
{
    public function test_user_login()
    {
        Artisan::call('migrate');

        $password = bcrypt('senha');

        User::create([
            'name' => 'Giovani',
            'email' => 'giovani@email.com',
            'permission' => 0,
            'password' => $password
        ]);

        $this->assertDatabaseHas('users', ['email' => 'giovani@email.com']);

        Session::start();
        $response = $this->withExceptionHandling()->call('POST', '/login', [
            'email' => 'giovani@email.com',
            'password' => 'senha',
            '_token' => csrf_token()
        ]);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertRedirect('/home');
    }

    public function test_user_without_registration_not_login()
    {
        Artisan::call('migrate');

        Session::start();
        $response = $this->withExceptionHandling()->call('POST', '/login', [
            'email' => 'notregistrated@email.com',
            'password' => 'notregis',
            '_token' => csrf_token()
        ]);

        $this->assertDatabaseMissing('users', ['email' => 'notregistrated@email.com']);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertRedirect('/');
    }

    public function test_users_without_registration_not_register_users()
    {
        Artisan::call('migrate');

        $response = $this->withExceptionHandling()->call('POST', '/user/register', [
            'name' => 'Notregistration',
            'email' => 'notregistratednot@email.com',
            'password' => 'notregisnot',
            'password_confirmation' => 'notregisnot',
            'permission' => 0,
            '_token' => csrf_token()
        ]);

        $this->assertDatabaseMissing('users', ['email' => 'notregistratednot@email.com']);      
    }  

    public function test_unregistered_users_not_insert_products()
    {
        Artisan::call('migrate');

        $supplier = factory('App\Supplier')->create();
        $response = $this->withExceptionHandling()->call('POST', '/product/insert', [
            'supplier' => ''.$supplier->id,
            'name'=>'Produto1',
            'description' => 'coisa estranha',
            'cost' => '20',
            'quantity' => '100',
            '_token' => csrf_token()
        ]);
        $response->assertStatus(403);

        $this->assertDatabaseMissing('products', 
            [
            'supplier_id' => ''.$supplier->id,
            'name' => 'Produto1',
            'description' => 'coisa estranha',
            'cost' => '20.0',
            'quantity' => '100',          
        ]);
    }

    public function test_unregistered_users_dont_insert_suppliers()
    {
        Artisan::call('migrate');
        
        $response = $this->withExceptionHandling()->call('POST','/supplier/insert', [
            'name' => 'Materiais Novos Ltda',
            'cnpj' => 111222333000118,
            'address' => 'Av. Paulo Faccini, 47',
            '_token' => csrf_token()
        ]);
        $response->assertStatus(403);
      
        $this->assertDatabaseMissing('suppliers', ['name' => 'Materiais Novos Ltda', 'cnpj' => 111222333000118, 'address' => 'Av. Paulo Faccini, 47']);
    }
}